---
layout: page
permalink: /
---

# COVID-19: Software Educativo Livre

Num contexto em que as atuais circunstâncias relacionadas com o COVID-19 obrigam alunos e professores a ficar em casa, a [ANSOL](https://ansol.org) apresenta uma lista de software livre que pode ser útil à comunidade estudantil.

Estes programas são livres e gratuitos, e a maior parte está disponível para diversos sistemas operativos.


**Ajude a melhorar esta lista. Envie as suas sugestões através [deste repositório](https://gitlab.com/ubuntu-pt/open-edu/).**

## A ser instalado pelas instituições

* [Big Blue Button](https://bigbluebutton.org) - Sistema de Web Conferencing para ensino à distância
* [Collabora Online](https://nextcloud.com/collaboraonline/) - Suite de Escritório na web
* [Mattermost](https://mattermost.com/) - Comunicação por escrito para equipas e comunidades
* [Moodle](https://moodle.org/?lang=pt) - Ensino à Distância
* [Mumble](https://www.mumble.com/) - Comunicação áudio de alta qualidade e baixa latência
* [Nextcloud](https://nextcloud.com/) - Gestão de documentos na cloud (pode incluir Office Online, videoconferência, etc.)
* [Omeka](https://omeka.org/) - Criação de exibições e colecções virtuais
* [Rocket.chat](https://rocket.chat/) - Comunicação por escrito para equipas e comunidades
* [Wordpress](https://wordpress.org/) - Plataforma de blogs
* [OpenVPN](https://openvpn.net/) - Acesso VPN

## A ser instalado pelos utilizadores

* [Calibre](https://calibre-ebook.com/) - Organização, edição, e catalogação de ebooks
* [FisicaLab](https://www.gnu.org/software/fisicalab/) - Resolução de problemas de Física
* [Framalibre](https://framasoft.org/en/) - Conjunto de ferramentas e recursos
* [GCompris](https://gcompris.net) - Actividades para crianças dos 2 aos 10 anos
* [Geogebra](https://www.geogebra.org/) - Aplicação de geometria, álgebra, estatística e cálculo
* [Gephi](https://gephi.org/) - Grafos
* [Ghostwriter](https://wereturtle.github.io/ghostwriter/) - Editor de markdown
* [Joplin](https://joplinapp.org/) - Tirar notas, com suporte de markdown e sincronização
* [LibreOffice](https://www.libreoffice.org/) - Suite de escritório (editor de texto, editor de folhas de cálculo, etc.)
* [Mathics](https://mathics.github.io/) - Álgebra
* [MuseScore](https://musescore.org) - Escrita e leitura de pautas musicais
* [Octave](https://www.gnu.org/software/octave/) - Linguagem semelhante ao MATLAB para computação numérica
* [Okular](https://okular.kde.org/) - Leitor e anotador de pdf e outros formatos
* [Palladio](https://hdlab.stanford.edu/palladio/) - Criação de visualizações de dados históricos complexos
* [Scilab e Xcos](https://www.scilab.org/) - Sistema similar ao MATLAB e Simulink
* [Solfege](https://www.gnu.org/software/solfege/) - Treino de ouvido e música
* [TextGrid](https://textgrid.de/en/) - Análise e edição de texto
* [TimelineJS](https://timeline.knightlab.com/) - Criação de timelines
* [Tracker](https://physlets.org/tracker/) – Análise e modelação física de vídeos
* [Zotero](https://www.zotero.org/) - Gestão de bibliografias

### Imagem
* [Blender](https://www.blender.org) - Modelação e Animação 3D
* [Darktable](https://darktable.org) - Edição e organização de fotografias
* [Digikam](https://www.digikam.org) - Edição e organização de fotografias
* [GIMP](https://www.gimp.org) - Edição de imagem bitmap
* [Inkscape](https://inkscape.org) - Desenho vectorial
* [Krita](https://krita.org) - Pintura digital
* [MyPaint](http://mypaint.org) - Pintura digital
* [OpenToonz](https://opentoonz.github.io/e/) - Animação 2D
* [Synfig](https://www.synfig.org) - Animação 2D
* [Tropy](https://tropy.org) - Gestão de imagens
* [Tux Paint](http://www.tuxpaint.org) - Ferramenta de pintura para crianças

### Audio e Vídeo
* [Ardour](http://ardour.org) - Gravação, edição e mistura de áudio
* [Audacity](https://www.audacityteam.org) - Edição áudio
* [Kdenlive](https://kdenlive.org) - Editor de vídeo
* [Natron](https://natrongithub.github.io) - Composição e colorização de vídeo
* [OBS Studio](https://obsproject.com) - Estúdio de gravação e emissão (streaming) de aulas e palestras online
* [Olive](https://olivevideoeditor.org) - Editor de video
* [OpenShot](https://www.openshot.org/pt/) - Editor de vídeo
* [Toonloop](http://www.toonloop.com/) - Animação stop-motion

### Colaboração
* [Atom Teletype](https://github.com/atom/teletype) - Editor de texto com extensão para edição simultânea
* [Jitsi Meet](https://meet.jit.si) - Video conferência no browser
* [KDE Connect](https://kdeconnect.kde.org) - Controlar o computador com o telemóvel em sistemas Linux KDE
* [Riot](https://riot.im/app/) - Comunicação em grupo na Web (texto, voz e video)
